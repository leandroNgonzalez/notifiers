import discovery.Notifier;
import notifiers.Telegram;
import notifiers.Whatsapp;

public class NotifyFactory {

    public enum Channel {Telegram, Whatsapp}

    public static Notifier create(Channel channel){
        Notifier notifier;
        switch (channel){
            case Telegram:
                notifier = new Telegram();
                break;
            case Whatsapp:
                notifier = new Whatsapp();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + channel);
        }
        return notifier;
    }



}
