package notifiers;

import discovery.Notifier;

public class Telegram implements Notifier {

    @Override
    public String send() {
        String msj = "Notificación enviada por Telegram.";
        return msj;
    }
}
