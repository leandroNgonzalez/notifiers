package notifiers;

import discovery.Notifier;

public class Whatsapp implements Notifier {

    @Override
    public String send() {
        String msj = "Notificación enviada por Whatsapp.";
        return msj;
    }

}
